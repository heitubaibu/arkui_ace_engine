/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "core/components_ng/pattern/navigation/navigation_layout_algorithm.h"

#include <cmath>

#include "base/geometry/dimension.h"
#include "base/geometry/ng/offset_t.h"
#include "base/geometry/ng/size_t.h"
#include "base/log/ace_trace.h"
#include "base/memory/ace_type.h"
#include "base/utils/utils.h"
#include "core/components/common/layout/constants.h"
#include "core/components_ng/base/frame_node.h"
#include "core/components_ng/layout/layout_algorithm.h"
#include "core/components_ng/pattern/linear_layout/linear_layout_property.h"
#include "core/components_ng/pattern/navigation/navigation_declaration.h"
#include "core/components_ng/pattern/navigation/navigation_group_node.h"
#include "core/components_ng/pattern/navigation/navigation_layout_property.h"
#include "core/components_ng/pattern/navigation/navigation_pattern.h"
#include "core/components_ng/pattern/navrouter/navdestination_group_node.h"
#include "core/components_ng/property/calc_length.h"
#include "core/components_ng/property/layout_constraint.h"
#include "core/components_ng/property/measure_property.h"
#include "core/components_ng/property/measure_utils.h"
#include "core/pipeline_ng/pipeline_context.h"

namespace OHOS::Ace::NG {

constexpr static int32_t PLATFORM_VERSION_TEN = 10;
constexpr Dimension WINDOW_WIDTH = 520.0_vp;

namespace {

void MeasureDivider(LayoutWrapper* layoutWrapper, const RefPtr<NavigationGroupNode>& hostNode,
    const RefPtr<NavigationLayoutProperty>& navigationLayoutProperty, const SizeF& dividerSize_)
{
    auto dividerNode = hostNode->GetDividerNode();
    CHECK_NULL_VOID(dividerNode);
    auto index = hostNode->GetChildIndexById(dividerNode->GetId());
    auto dividerWrapper = layoutWrapper->GetOrCreateChildByIndex(index);
    CHECK_NULL_VOID(dividerWrapper);
    auto constraint = navigationLayoutProperty->CreateChildConstraint();
    constraint.selfIdealSize = OptionalSizeF(dividerSize_.Width(), dividerSize_.Height());
    dividerWrapper->Measure(constraint);
}

void MeasureContentChild(LayoutWrapper* layoutWrapper, const RefPtr<NavigationGroupNode>& hostNode,
    const RefPtr<NavigationLayoutProperty>& navigationLayoutProperty, const SizeF& contentSize_)
{
    auto contentNode = hostNode->GetContentNode();
    CHECK_NULL_VOID(contentNode);
    auto index = hostNode->GetChildIndexById(contentNode->GetId());
    auto contentWrapper = layoutWrapper->GetOrCreateChildByIndex(index);
    CHECK_NULL_VOID(contentWrapper);
    auto constraint = navigationLayoutProperty->CreateChildConstraint();
    constraint.selfIdealSize = OptionalSizeF(contentSize_.Width(), contentSize_.Height());
    contentWrapper->Measure(constraint);
}

float LayoutNavBar(LayoutWrapper* layoutWrapper, const RefPtr<NavigationGroupNode>& hostNode,
    const RefPtr<NavigationLayoutProperty>& navigationLayoutProperty, const NavBarPosition& position)
{
    if (navigationLayoutProperty->GetHideNavBar().value_or(false) &&
        navigationLayoutProperty->GetNavigationModeValue() == NavigationMode::SPLIT) {
        return 0.0f;
    }

    auto contentNode = hostNode->GetContentNode();
    CHECK_NULL_RETURN(contentNode, 0.0f);
    auto navBarNode = hostNode->GetNavBarNode();
    CHECK_NULL_RETURN(navBarNode, 0.0f);
    auto index = hostNode->GetChildIndexById(navBarNode->GetId());
    auto navBarWrapper = layoutWrapper->GetOrCreateChildByIndex(index);
    CHECK_NULL_RETURN(navBarWrapper, 0.0f);
    auto geometryNode = navBarWrapper->GetGeometryNode();
    auto navigationGeometryNode = layoutWrapper->GetGeometryNode();
    if (position == NavBarPosition::END) {
        auto navBarOffset =
            OffsetT<float>(navigationGeometryNode->GetFrameSize().Width() - geometryNode->GetFrameSize().Width(),
                geometryNode->GetFrameOffset().GetY());
        geometryNode->SetMarginFrameOffset(navBarOffset);
        navBarWrapper->Layout();
        return geometryNode->GetFrameSize().Width();
    }
    if (hostNode->GetIsModeChange() &&
        (navigationLayoutProperty->GetDestinationChange().value_or(false) ||
            contentNode->FindChildNodeOfClass<NavDestinationGroupNode>()) &&
        navigationLayoutProperty->GetNavigationMode() == NavigationMode::STACK) {
        auto contentOffset = OffsetT<float>(-geometryNode->GetFrameSize().Width(), 0.0f);
        geometryNode->SetMarginFrameOffset(contentOffset);
        navBarWrapper->Layout();
        return geometryNode->GetFrameSize().Width();
    }
    auto navBarOffset = OffsetT<float>(0.0f, 0.0f);
    geometryNode->SetMarginFrameOffset(navBarOffset);
    navBarWrapper->Layout();
    return geometryNode->GetFrameSize().Width();
}

float LayoutDivider(LayoutWrapper* layoutWrapper, const RefPtr<NavigationGroupNode>& hostNode,
    const RefPtr<NavigationLayoutProperty>& navigationLayoutProperty, float navBarWidth, const NavBarPosition& position)
{
    auto dividerNode = hostNode->GetDividerNode();
    CHECK_NULL_RETURN(dividerNode, 0.0f);
    auto index = hostNode->GetChildIndexById(dividerNode->GetId());
    auto dividerWrapper = layoutWrapper->GetOrCreateChildByIndex(index);
    CHECK_NULL_RETURN(dividerWrapper, 0.0f);
    auto geometryNode = dividerWrapper->GetGeometryNode();
    auto navigationGeometryNode = layoutWrapper->GetGeometryNode();
    OffsetT<float> dividerOffset;
    if (position == NavBarPosition::END) {
        dividerOffset = OffsetT<float>(
            navigationGeometryNode->GetFrameSize().Width() - geometryNode->GetFrameSize().Width() - navBarWidth,
            geometryNode->GetFrameOffset().GetY());
    } else {
        dividerOffset = OffsetT<float>(navBarWidth, geometryNode->GetFrameOffset().GetY());
    }
    geometryNode->SetMarginFrameOffset(dividerOffset);
    dividerWrapper->Layout();
    return geometryNode->GetFrameSize().Width();
}

void LayoutContent(LayoutWrapper* layoutWrapper, const RefPtr<NavigationGroupNode>& hostNode,
    const RefPtr<NavigationLayoutProperty>& navigationLayoutProperty, float navBarWidth, float dividerWidth,
    const NavBarPosition& position)
{
    auto contentNode = hostNode->GetContentNode();
    CHECK_NULL_VOID(contentNode);
    auto index = hostNode->GetChildIndexById(contentNode->GetId());
    auto contentWrapper = layoutWrapper->GetOrCreateChildByIndex(index);
    CHECK_NULL_VOID(contentWrapper);
    auto geometryNode = contentWrapper->GetGeometryNode();
    CHECK_NULL_VOID(geometryNode);
    const auto& navigationGeometryNode = layoutWrapper->GetGeometryNode();
    CHECK_NULL_VOID(navigationGeometryNode);

    OffsetT<float> contentOffset;
    if ((navigationLayoutProperty->GetDestinationChange().value_or(false) ||
            contentNode->FindChildNodeOfClass<NavDestinationGroupNode>()) &&
        navigationLayoutProperty->GetNavigationMode() == NavigationMode::STACK) {
        contentOffset = OffsetT<float>(0.0f, 0.0f);
        geometryNode->SetMarginFrameOffset(contentOffset);
        contentWrapper->Layout();
        return;
    }
    if (position == NavBarPosition::END) {
        navigationLayoutProperty->GetNavigationMode() == NavigationMode::STACK
            ? contentOffset = OffsetT<float>(-geometryNode->GetFrameSize().Width(), 0.0f)
            : contentOffset = OffsetT<float>(0.0f, 0.0f);
        geometryNode->SetMarginFrameOffset(contentOffset);
        contentWrapper->Layout();
        return;
    }
    navigationLayoutProperty->GetNavigationMode() == NavigationMode::STACK
        ? contentOffset =
              OffsetT<float>(navigationGeometryNode->GetFrameSize().Width(), geometryNode->GetFrameOffset().GetY())
        : contentOffset = OffsetT<float>(navBarWidth + dividerWidth, geometryNode->GetFrameOffset().GetY());
    geometryNode->SetMarginFrameOffset(contentOffset);
    contentWrapper->Layout();
}

void FitScrollFullWindow(SizeF& frameSize)
{
    auto pipeline = PipelineContext::GetCurrentContext();
    CHECK_NULL_VOID_NOLOG(pipeline);
    if (frameSize.Width() == Infinity<float>()) {
        frameSize.SetWidth(pipeline->GetRootWidth());
    }
    if (frameSize.Height() == Infinity<float>()) {
        frameSize.SetHeight(pipeline->GetRootHeight());
    }
}

} // namespace

void NavigationLayoutAlgorithm::GetRange(const RefPtr<NavigationGroupNode>& hostNode)
{
    auto navigationPattern = AceType::DynamicCast<NavigationPattern>(hostNode->GetPattern());
    CHECK_NULL_VOID(navigationPattern);
    minNavBarWidthValue_ = navigationPattern->GetMinNavBarWidthValue();
    maxNavBarWidthValue_ = navigationPattern->GetMaxNavBarWidthValue();
    minContentWidthValue_ = navigationPattern->GetMinContentWidthValue();
    userSetNavBarRangeFlag_ = navigationPattern->GetUserSetNavBarRangeFlag();
    userSetMinContentFlag_ = navigationPattern->GetUserSetMinContentFlag();
}

void NavigationLayoutAlgorithm::UpdateNavigationMode(LayoutWrapper* layoutWrapper,
    const RefPtr<NavigationGroupNode>& hostNode, const RefPtr<NavigationLayoutProperty>& navigationLayoutProperty,
    const SizeF& frameSize)
{
    auto lastNavMode = navigationLayoutProperty->GetNavigationModeValue(NavigationMode::AUTO);
    usrNavigationMode_ = navigationLayoutProperty->GetUsrNavigationModeValue(NavigationMode::AUTO);
    navigationMode_ = usrNavigationMode_;
    auto navigationPattern = AceType::DynamicCast<NavigationPattern>(hostNode->GetPattern());
    CHECK_NULL_VOID(navigationPattern);
    auto pipeline = PipelineContext::GetCurrentContext();
    CHECK_NULL_VOID(pipeline);
    auto currentPlatformVersion = pipeline->GetMinPlatformVersion();

    auto navigationWidth = 0.0f;

    if (currentPlatformVersion >= PLATFORM_VERSION_TEN) {
        navigationWidth = static_cast<float>(minNavBarWidthValue_.ConvertToPx() + minContentWidthValue_.ConvertToPx());
    } else {
        navigationWidth = static_cast<float>(WINDOW_WIDTH.ConvertToPx());
    }

    if (navigationMode_ == NavigationMode::AUTO) {
        if (frameSize.Width() >= navigationWidth) {
            navigationMode_ = NavigationMode::SPLIT;
            navigationLayoutProperty->UpdateNavigationMode(navigationMode_);
            navigationPattern->SetNavigationMode(navigationMode_);
        } else {
            navigationMode_ = NavigationMode::STACK;
            navigationLayoutProperty->UpdateNavigationMode(navigationMode_);
            navigationPattern->SetNavigationMode(navigationMode_);
        }

    } else {
        auto layoutAlgorithmWrapper = DynamicCast<LayoutAlgorithmWrapper>(layoutWrapper->GetLayoutAlgorithm());
        CHECK_NULL_VOID(layoutAlgorithmWrapper);
        auto navigationLayoutAlgorithm =
            DynamicCast<NavigationLayoutAlgorithm>(layoutAlgorithmWrapper->GetLayoutAlgorithm());
        navigationLayoutProperty->UpdateNavigationMode(navigationMode_);
        navigationLayoutAlgorithm->SetNavigationMode(navigationMode_);
        navigationPattern->SetNavigationMode(navigationMode_);
    }
    if (lastNavMode == NavigationMode::AUTO) {
        navigationPattern->SetNavModeChange(navigationMode_ == NavigationMode::SPLIT);
    } else {
        navigationPattern->SetNavModeChange(navigationMode_ != lastNavMode);
    }
}

void NavigationLayoutAlgorithm::SizeCalculation(const RefPtr<NavigationGroupNode>& hostNode,
    const RefPtr<NavigationLayoutProperty>& navigationLayoutProperty, const SizeF& frameSize)
{
    auto pipeline = PipelineContext::GetCurrentContext();
    CHECK_NULL_VOID(pipeline);
    auto currentPlatformVersion = pipeline->GetMinPlatformVersion();
    auto constraint = navigationLayoutProperty->GetLayoutConstraint();
    auto parentSize = CreateIdealSize(constraint.value(), Axis::HORIZONTAL, MeasureType::MATCH_PARENT);

    if (currentPlatformVersion >= PLATFORM_VERSION_TEN) {
        auto minNavBarWidth = minNavBarWidthValue_.ConvertToPxWithSize(parentSize.Width().value_or(0.0f));
        auto maxNavBarWidth = maxNavBarWidthValue_.ConvertToPxWithSize(parentSize.Width().value_or(0.0f));
        realNavBarWidth_ = std::max(realNavBarWidth_, static_cast<float>(minNavBarWidth));
        realNavBarWidth_ = std::min(realNavBarWidth_, static_cast<float>(maxNavBarWidth));
    } else {
        auto navBarWidthValue = navigationLayoutProperty->GetNavBarWidthValue(DEFAULT_NAV_BAR_WIDTH);
        auto navBarWidth = navBarWidthValue.ConvertToPxWithSize(parentSize.Width().value_or(0.0f));
        realNavBarWidth_ = navBarWidth;
    }
    navBarSize_ = frameSize;
    contentSize_ = frameSize;
    dividerSize_ = SizeF(0.0f, frameSize.Height());

    if (navigationLayoutProperty->GetNavigationModeValue(navigationMode_) == NavigationMode::SPLIT) {
        SizeCalculationSplit(navigationLayoutProperty, frameSize);
    } else {
        SizeCalculationStack(hostNode, navigationLayoutProperty, frameSize);
    }

    navBarSize_.SetWidth(realNavBarWidth_);
    dividerSize_.SetWidth(realDividerWidth_);
    contentSize_.SetWidth(realContentWidth_);
}

void NavigationLayoutAlgorithm::SizeCalculationSplit(
    const RefPtr<NavigationLayoutProperty>& navigationLayoutProperty, const SizeF& frameSize)
{
    float frameWidth = frameSize.Width();
    auto constraint = navigationLayoutProperty->GetLayoutConstraint();
    auto parentSize = CreateIdealSize(constraint.value(), Axis::HORIZONTAL, MeasureType::MATCH_PARENT);
    auto dividerWidth = static_cast<float>(DIVIDER_WIDTH.ConvertToPx());
    auto minContentWidth = minContentWidthValue_.ConvertToPxWithSize(parentSize.Width().value_or(0.0f));
    realContentWidth_ = minContentWidth;

    if (navigationLayoutProperty->GetHideNavBar().value_or(false)) {
        navBarSize_ = SizeF(0.0f, 0.0f);
        dividerSize_ = SizeF(0.0f, 0.0f);
        realNavBarWidth_ = 0.0f;
        realContentWidth_ = frameWidth;
    } else {
        if (userSetMinContentFlag_ && !userSetNavBarRangeFlag_) {
            if (minContentWidth >= frameWidth) {
                realContentWidth_ = frameWidth;
                realNavBarWidth_ = 0.0f;
            } else if (realNavBarWidth_ + dividerWidth + minContentWidth <= frameWidth) {
                realContentWidth_ = frameWidth - realNavBarWidth_ - dividerWidth;
            } else {
                realContentWidth_ = minContentWidth;
                realNavBarWidth_ = frameWidth - realContentWidth_ - dividerWidth;
            }
        } else {
            float remainingSpace = frameWidth - realNavBarWidth_ - dividerWidth;
            auto minNavBarWidth = minNavBarWidthValue_.ConvertToPxWithSize(parentSize.Width().value_or(0.0f));
            if (remainingSpace >= minContentWidth) {
                realContentWidth_ = remainingSpace;
            } else if (remainingSpace < minContentWidth && realNavBarWidth_ > minNavBarWidth) {
                realContentWidth_ = minContentWidth;
                realNavBarWidth_ = frameWidth - minContentWidth - dividerWidth;
            } else {
                realNavBarWidth_ = minNavBarWidth;
                realContentWidth_ = frameWidth - minNavBarWidth - dividerWidth;
            }
        }
    }

    realDividerWidth_ = std::max(realDividerWidth_, 0.0f);
    realContentWidth_ = std::max(realContentWidth_, 0.0f);
    realNavBarWidth_ = std::min(realNavBarWidth_, frameWidth);
    realContentWidth_ = std::min(realContentWidth_, frameWidth);
    if (realNavBarWidth_ == 0.0f || realContentWidth_ == 0.0f) {
        realDividerWidth_ = 0.0f;
    } else {
        realDividerWidth_ = dividerWidth;
    }
}

void NavigationLayoutAlgorithm::SizeCalculationStack(const RefPtr<NavigationGroupNode>& hostNode,
    const RefPtr<NavigationLayoutProperty>& navigationLayoutProperty, const SizeF& frameSize)
{
    auto contentNode = hostNode->GetContentNode();
    CHECK_NULL_VOID(contentNode);
    realDividerWidth_ = 0.0f;
    float frameWidth = frameSize.Width();
    if (navigationLayoutProperty->GetDestinationChange().value_or(false) ||
        contentNode->FindChildNodeOfClass<NavDestinationGroupNode>()) {
        realContentWidth_ = frameWidth;
        realNavBarWidth_ = 0.0f;
    } else {
        realContentWidth_ = 0.0f;
        realNavBarWidth_ = frameWidth;
    }
    auto usrNavigationMode_ = navigationLayoutProperty->GetUsrNavigationModeValue(NavigationMode::AUTO);
    if (usrNavigationMode_ == NavigationMode::AUTO && hostNode->GetIsModeChange() &&
        navigationLayoutProperty->GetNavigationMode() == NavigationMode::STACK) {
        realContentWidth_ = 0.0f;
        realNavBarWidth_ = frameWidth;
    }
}

void NavigationLayoutAlgorithm::MeasureNavBar(LayoutWrapper* layoutWrapper, const RefPtr<NavigationGroupNode>& hostNode,
    const RefPtr<NavigationLayoutProperty>& navigationLayoutProperty, const SizeF& navBarSize_)
{
    auto navBarNode = hostNode->GetNavBarNode();
    CHECK_NULL_VOID(navBarNode);
    auto index = hostNode->GetChildIndexById(navBarNode->GetId());
    auto navBarWrapper = layoutWrapper->GetOrCreateChildByIndex(index);
    CHECK_NULL_VOID(navBarWrapper);
    auto constraint = navigationLayoutProperty->CreateChildConstraint();
    constraint.selfIdealSize = OptionalSizeF(navBarSize_.Width(), navBarSize_.Height());
    navBarWrapper->Measure(constraint);
    realNavBarHeight_ = navBarWrapper->GetGeometryNode()->GetFrameSize().Height();
}

void NavigationLayoutAlgorithm::Measure(LayoutWrapper* layoutWrapper)
{
    auto hostNode = AceType::DynamicCast<NavigationGroupNode>(layoutWrapper->GetHostNode());
    CHECK_NULL_VOID(hostNode);
    auto navigationLayoutProperty = AceType::DynamicCast<NavigationLayoutProperty>(layoutWrapper->GetLayoutProperty());
    CHECK_NULL_VOID(navigationLayoutProperty);
    const auto& constraint = navigationLayoutProperty->GetLayoutConstraint();
    CHECK_NULL_VOID(constraint);
    auto geometryNode = layoutWrapper->GetGeometryNode();
    auto size = CreateIdealSize(constraint.value(), Axis::HORIZONTAL, MeasureType::MATCH_PARENT, true);
    FitScrollFullWindow(size);
    const auto& padding = layoutWrapper->GetLayoutProperty()->CreatePaddingAndBorder();
    MinusPaddingToSize(padding, size);
    if (size.Width() == 0.0f) {
        return;
    }
    GetRange(hostNode);
    UpdateNavigationMode(layoutWrapper, hostNode, navigationLayoutProperty, size);
    SizeCalculation(hostNode, navigationLayoutProperty, size);

    MeasureNavBar(layoutWrapper, hostNode, navigationLayoutProperty, navBarSize_);
    MeasureContentChild(layoutWrapper, hostNode, navigationLayoutProperty, contentSize_);
    MeasureDivider(layoutWrapper, hostNode, navigationLayoutProperty, dividerSize_);
    layoutWrapper->GetGeometryNode()->SetFrameSize(size);
}

void NavigationLayoutAlgorithm::Layout(LayoutWrapper* layoutWrapper)
{
    auto hostNode = AceType::DynamicCast<NavigationGroupNode>(layoutWrapper->GetHostNode());
    CHECK_NULL_VOID(hostNode);
    auto navigationLayoutProperty = AceType::DynamicCast<NavigationLayoutProperty>(layoutWrapper->GetLayoutProperty());
    CHECK_NULL_VOID(navigationLayoutProperty);
    auto navBarPosition = navigationLayoutProperty->GetNavBarPositionValue(NavBarPosition::START);
    float navBarWidth = LayoutNavBar(layoutWrapper, hostNode, navigationLayoutProperty, navBarPosition);
    float dividerWidth = LayoutDivider(layoutWrapper, hostNode, navigationLayoutProperty, navBarWidth, navBarPosition);
    LayoutContent(layoutWrapper, hostNode, navigationLayoutProperty, navBarWidth, dividerWidth, navBarPosition);
}

} // namespace OHOS::Ace::NG
